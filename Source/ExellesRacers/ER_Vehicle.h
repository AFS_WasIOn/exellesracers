// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/TextRenderComponent.h"
#include "ChaosVehicles/Public/WheeledVehiclePawn.h"
#include "ER_Vehicle.generated.h"

/**
 *
 */

class UCameraComponent;
class USpringArmComponent;
class UTextRenderComponent;
class UInputComponent;

USTRUCT(BlueprintType)
struct FWheelStruct
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMesh* WheelMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Friction;
};


UCLASS()
class EXELLESRACERS_API AER_Vehicle : public AWheeledVehiclePawn
{
	GENERATED_BODY()

public:
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly)
		USpringArmComponent* SpringArm;

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly)
		UCameraComponent* Camera;

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly)
		USceneComponent* InternalCameraBase;

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly)
		UCameraComponent* InternalCamera;

	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
		FText SpeedDisplayString;

	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
		FText GearDisplayString;

	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
		FColor	GearDisplayColor;

	UPROPERTY(Category = Display, VisibleDefaultsOnly, BlueprintReadOnly)
		FColor	GearDisplayReverseColor;

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly)
		bool bInCarCameraActive;

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly)
		bool bInReverseGear;

	UPROPERTY(Category = System, EditAnywhere, BlueprintReadWrite)
		TArray<UStaticMeshComponent*> Wheels;
	
	UPROPERTY(Category = System, EditAnywhere, BlueprintReadWrite)
		float EngineMultiplier;
	
	UPROPERTY(Category = System, EditAnywhere, BlueprintReadWrite)
		float ControlMultiplier;
	
	UPROPERTY(Category = System, EditAnywhere, BlueprintReadWrite)
		FColor CurrentColor;
	
	UPROPERTY(Category = Customization, EditAnywhere, BlueprintReadWrite)
		FString CarName;

	FVector InternalCameraOrigin;

	UFUNCTION(Category = Customization, BlueprintCallable)
		void SetWheel(FWheelStruct newWheel);
	
	UFUNCTION(Category = Customization, BlueprintCallable)
		void UpdateEngine(float EM = 1.0f, float Control = 1.0f);
	
	UFUNCTION(Category = Customization, BlueprintCallable)
		void UpdateColor(FColor newColor);
	
	AER_Vehicle();
	
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

	virtual void Tick(float Delta) override;
	
	virtual void BeginPlay() override;

	void MoveForward(float Val);
	
	void MoveRight(float Val);
	
	void LookUD(float Val);
	void LookRL(float Val);
	
	void OnHandbrakePressed();
	
	void OnHandbrakeReleased();
	
	void OnToggleCamera();

	void EnableIncarView(const bool bState, const bool bForce = false);

	void UpdateHUDStrings();

	FORCEINLINE USpringArmComponent* GetSpringArm() const { return SpringArm; }
	FORCEINLINE UCameraComponent* GetCamera() const { return Camera; }
	FORCEINLINE UCameraComponent* GetInternalCamera() const { return InternalCamera; }

};
