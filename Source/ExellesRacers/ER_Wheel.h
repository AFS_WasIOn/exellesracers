// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ChaosVehicleWheel.h"
#include "ER_Wheel.generated.h"

/**
 * 
 */
UCLASS()
class EXELLESRACERS_API UER_Wheel : public UChaosVehicleWheel
{
	GENERATED_BODY()

public:
	
		UER_Wheel();
	
};
