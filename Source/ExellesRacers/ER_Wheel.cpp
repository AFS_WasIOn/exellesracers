// Fill out your copyright notice in the Description page of Project Settings.


#include "ER_Wheel.h"

UER_Wheel::UER_Wheel()
{
	WheelRadius = 85.0f;
	WheelWidth = 20.0f;
	MaxBrakeTorque = 2000.0f;
	MaxHandBrakeTorque = 5000.0f;
	bAffectedByHandbrake = true;
	bAffectedByEngine = true;
	AxleType = EAxleType::Rear;
	LateralFrictionForceMultiplier = 5.0f;
}


