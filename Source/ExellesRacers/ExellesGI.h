// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ExellesGI.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum CBLevel
{
	Low,
	Medium,
	High,
	Ultra
};

UCLASS()
class EXELLESRACERS_API UExellesGI : public UGameInstance
{
	GENERATED_BODY()

public:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bShowHints;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
			float GeneralSounds;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
			float EffectsSounds;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
			float MusicSounds;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
			FName NextMapName;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
			TEnumAsByte<CBLevel> GeneralQuality;
	
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
			TEnumAsByte<CBLevel> TextureQuality;
	
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
			TEnumAsByte<CBLevel> EffectsQuality;
	
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "System")
			bool bPlaySolo;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "System")
			bool bHost;
};
