// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ER_Wheel.h"
#include "ER_FrontWheel.generated.h"

/**
 * 
 */
UCLASS()
class EXELLESRACERS_API UER_FrontWheel : public UER_Wheel
{
	GENERATED_BODY()

		UER_FrontWheel();
};
