// Fill out your copyright notice in the Description page of Project Settings.


#include "ER_Vehicle.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "WheeledVehicleMovementComponent4W.h"
#include "Engine/SkeletalMesh.h"
#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/TextRenderComponent.h"
#include "Materials/Material.h"
#include "GameFramework/Controller.h"
#include "ChaosVehicles/Public/ChaosVehicleMovementComponent.h"
#include "ChaosVehicles/Public/ChaosWheeledVehicleMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Materials/MaterialInstanceDynamic.h"

AER_Vehicle::AER_Vehicle()
{
	Wheels.Add(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FL")));
	Wheels.Add(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FR")));
	Wheels.Add(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RL")));
	Wheels.Add(CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RR")));
	Wheels[0]->SetupAttachment(GetMesh(), FName("FL"));
	Wheels[1]->SetupAttachment(GetMesh(), FName("FR"));
	Wheels[2]->SetupAttachment(GetMesh(), FName("RL"));
	Wheels[3]->SetupAttachment(GetMesh(), FName("RR"));
	for(int i = 0; i < 4; i++)
	{
		Wheels[i]->SetRelativeLocation(FVector::ZeroVector);
	}

	EngineMultiplier = 1.0f;
	ControlMultiplier = 1.0f;

	// Create a spring arm component
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm0"));
	SpringArm->TargetOffset = FVector(0.f, 0.f, 200.f);
	SpringArm->SetRelativeRotation(FRotator(-15.f, 0.f, 0.f));
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 600.0f;
	SpringArm->bEnableCameraRotationLag = true;
	SpringArm->CameraRotationLagSpeed = 7.f;
	SpringArm->bInheritPitch = false;
	SpringArm->bInheritRoll = false;

	// Create camera component 
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera0"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false;
	Camera->FieldOfView = 90.f;

	// Create In-Car camera component 
	InternalCameraOrigin = FVector(0.0f, -40.0f, 120.0f);

	InternalCameraBase = CreateDefaultSubobject<USceneComponent>(TEXT("InternalCameraBase"));
	InternalCameraBase->SetRelativeLocation(InternalCameraOrigin);
	InternalCameraBase->SetupAttachment(GetMesh());

	InternalCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("InternalCamera"));
	InternalCamera->bUsePawnControlRotation = false;
	InternalCamera->FieldOfView = 90.f;
	InternalCamera->SetupAttachment(InternalCameraBase);

	//Setup TextRenderMaterial
	static ConstructorHelpers::FObjectFinder<UMaterial> TextMaterial(TEXT("Material'/Engine/EngineMaterials/AntiAliasedTextMaterialTranslucent.AntiAliasedTextMaterialTranslucent'"));

	UMaterialInterface* Material = TextMaterial.Object;

	// Colors for the incar gear display. One for normal one for reverse
	GearDisplayReverseColor = FColor(255, 0, 0, 255);
	GearDisplayColor = FColor(255, 255, 255, 255);

	// Colors for the in-car gear display. One for normal one for reverse
	GearDisplayReverseColor = FColor(255, 0, 0, 255);
	GearDisplayColor = FColor(255, 255, 255, 255);

	bInReverseGear = false;
}

void AER_Vehicle::SetWheel(FWheelStruct w)
{
	if(w.WheelMesh == NULL)
	{
		UE_LOG(LogTemp, Warning, TEXT("WheelSetup mesh is null"));
		return;
	}
	
	for (UStaticMeshComponent* El : Wheels)
	{
		El->SetStaticMesh(w.WheelMesh);
		El->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		El->SetRelativeScale3D(FVector::OneVector);
	}
	UChaosWheeledVehicleMovementComponent* wmc = Cast<UChaosWheeledVehicleMovementComponent>(GetVehicleMovementComponent());

	if (wmc == NULL)
	{
		UE_LOG(LogTemp, Warning, TEXT("Can't cast to UCWVMC"));
		return;
	}
	for(UChaosVehicleWheel* El : wmc->Wheels)
	{
		El->LongitudinalFrictionForceMultiplier = w.Friction;
		El->LateralFrictionForceMultiplier = w.Friction;
		UE_LOG(LogTemp, Warning, TEXT("%f"),w.Friction);
	}
}

void AER_Vehicle::UpdateEngine(float EM, float Control)
{
	UChaosWheeledVehicleMovementComponent* wmc = Cast<UChaosWheeledVehicleMovementComponent>(GetVehicleMovementComponent());
	EngineMultiplier = EM;
	ControlMultiplier = Control;
	
	if (wmc == NULL)
	{
		UE_LOG(LogTemp, Warning, TEXT("Can't cast to UCWVMC"));
		return;
	}
	if(ControlMultiplier != 0.0f)
	{
		wmc->SteeringSetup.AngleRatio = 0.25 + 1.2f * ControlMultiplier;
	}
	wmc->EngineSetup.MaxTorque = 1100.0f + 1200.0f * EM;
	wmc->EngineSetup.EngineIdleRPM = 1200.0f + 800.0f * EM;
	wmc->EngineSetup.EngineRevUpMOI = 400.0f + 1000.0f * EM;
	wmc->EngineSetup.MaxRPM = 3000 + 1800 * EM;
	wmc->TransmissionSetup.ChangeUpRPM = 1600.0f + 800.0f * EM;
	wmc->TransmissionSetup.ChangeDownRPM = 1000.0f + 500.0f * EM;
	wmc->RecreatePhysicsState();
}

void AER_Vehicle::UpdateColor(FColor newColor)
{
	CurrentColor = newColor;
	UMaterialInterface* m = GetMesh()->GetMaterial(0);
	UMaterialInstanceDynamic* Mat = UMaterialInstanceDynamic::Create(m, this);
	Mat->SetVectorParameterValue("Color", CurrentColor);
	GetMesh()->SetMaterial(0, Mat);
}

void AER_Vehicle::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AER_Vehicle::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AER_Vehicle::MoveRight);
	PlayerInputComponent->BindAxis("LookUD", this, &AER_Vehicle::LookUD);
	PlayerInputComponent->BindAxis("LookRL", this, &AER_Vehicle::LookRL);
	PlayerInputComponent->BindAxis("LookUp");
	PlayerInputComponent->BindAxis("LookRight");

	PlayerInputComponent->BindAction("Handbrake", IE_Pressed, this, &AER_Vehicle::OnHandbrakePressed);
	PlayerInputComponent->BindAction("Handbrake", IE_Released, this, &AER_Vehicle::OnHandbrakeReleased);
	PlayerInputComponent->BindAction("SwitchCamera", IE_Pressed, this, &AER_Vehicle::OnToggleCamera);
}

void AER_Vehicle::MoveForward(float Val)
{
	if(Val >= 0)
	{
		bInReverseGear = false;
		GetVehicleMovementComponent()->SetThrottleInput(Val);
		GetVehicleMovementComponent()->SetBrakeInput(0.0f);
	}
	else
	{
		bInReverseGear = true;
		GetVehicleMovementComponent()->SetBrakeInput(abs(Val));
		GetVehicleMovementComponent()->SetThrottleInput(0.0f);
	}
}

void AER_Vehicle::LookRL(float Val)
{
	AddControllerYawInput(Val);
}

void AER_Vehicle::LookUD(float Val)
{
	AddControllerPitchInput(Val);
}



void AER_Vehicle::MoveRight(float Val)
{
	GetVehicleMovementComponent()->SetSteeringInput(Val);
}

void AER_Vehicle::OnHandbrakePressed()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(true);
}

void AER_Vehicle::OnHandbrakeReleased()
{
	GetVehicleMovementComponent()->SetHandbrakeInput(false);
}

void AER_Vehicle::OnToggleCamera()
{
	EnableIncarView(!bInCarCameraActive);
}

void AER_Vehicle::EnableIncarView(const bool bState, const bool bForce)
{
	if ((bState != bInCarCameraActive) || (bForce == true))
	{
		bInCarCameraActive = bState;

		if (bState == true)
		{
			Camera->Deactivate();
			InternalCamera->Activate();
		}
		else
		{
			InternalCamera->Deactivate();
			Camera->Activate();
		}
	}
}


void AER_Vehicle::Tick(float Delta)
{
	Super::Tick(Delta);

	// Setup the flag to say we are in reverse gear
	bInReverseGear = GetVehicleMovement()->GetCurrentGear() < 0;

	// Update the strings used in the hud (incar and onscreen)
	UpdateHUDStrings();

	// Set the string in the incar hud
}

void AER_Vehicle::BeginPlay()
{
	Super::BeginPlay();

	bool bEnableInCar = false;
	
	EnableIncarView(bEnableInCar, true);
}

void AER_Vehicle::UpdateHUDStrings()
{/*
	float KPH = FMath::Abs(GetVehicleMovement()->GetForwardSpeed()) * 0.036f;
	int32 KPH_int = FMath::FloorToInt(KPH);

	// Using FText because this is display text that should be localizable
	SpeedDisplayString = FText::Format(LOCTEXT("SpeedFormat", "{0} km/h"), FText::AsNumber(KPH_int));

	if (bInReverseGear == true)
	{
		GearDisplayString = FText(LOCTEXT("ReverseGear", "R"));
	}
	else
	{
		int32 Gear = GetVehicleMovement()->GetCurrentGear();
		GearDisplayString = (Gear == 0) ? LOCTEXT("N", "N") : FText::AsNumber(Gear);
	}*/
}