// Fill out your copyright notice in the Description page of Project Settings.


#include "ER_FrontWheel.h"

UER_FrontWheel::UER_FrontWheel()
{
	bAffectedByHandbrake = false;
	bAffectedBySteering = true;
	bAffectedByEngine = false;
	MaxSteerAngle = 60.0f;
	AxleType = EAxleType::Front;
}

